package com.epam.automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LandingPage extends SimplePage {
    public String pageURL;

    public LandingPage(){
        this.pageURL = "http://www.mackiev.com/";
    }

    public WhereToBuyPage goToStore(){
        this.getDriver().findElement(By.className("item-wtb ")).click();

        WebDriverWait wait = new WebDriverWait(this.getDriver(), this.timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("reseller-title")));

        return new WhereToBuyPage();
    }

    public LandingPage openPage(){
        this.getDriver().get(this.pageURL);
        return this;
    }
}
