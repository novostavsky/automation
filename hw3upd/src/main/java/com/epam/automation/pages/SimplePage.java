package com.epam.automation.pages;

import com.epam.automation.utils.SimpleDriver;
import org.openqa.selenium.WebDriver;

public class SimplePage {
    private WebDriver driver;
    protected String pageURL;
    protected int timeoutInSeconds;

    public SimplePage() {
        this.driver = SimpleDriver.getDriver();
        this.pageURL = "http://www.mackiev.com/";
        this.timeoutInSeconds = 10;
    }

    public WebDriver getDriver(){
        return this.driver;
    }

    public SimplePage openPage(){
        this.getDriver().get(this.pageURL);
        return this;
    }

    public boolean isTextPresent(String textToCheck){
        return this.getDriver().getPageSource().contains(textToCheck);
    }
}
