package com.epam.automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class USStorePage extends SimplePage {
    public String pageURL;

    public USStorePage(){
        this.pageURL = "http://www.mackiev.com/store/store_us.html";
    }

    public USStorePage selectState(String stateName){
        this.getDriver().findElement(By.id("l_state")).sendKeys(stateName);
        return this;
    }

}
