package com.epam.automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WhereToBuyPage extends SimplePage{
    public String pageURL;

    public WhereToBuyPage(){
        this.pageURL = "http://www.mackiev.com/wheretobuy.html";
    }

    public USStorePage openUSStorePage(){
        this.getDriver().findElement(By.className("flag-usa")).click();

        WebDriverWait wait = new WebDriverWait(this.getDriver(), this.timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("content")));

        return new USStorePage();
    }

}
