package com.epam.automation.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.io.File;
import java.io.IOException;

public class SimpleDriver {
    private static WebDriver driver = null;

    private SimpleDriver() {
        EventFiringWebDriver eDriver = null;

        ChromeDriverService service = new ChromeDriverService.Builder()
                .usingDriverExecutable(new File("chromedriver.exe"))
                .usingAnyFreePort()
                .build();
        try {
             service.start();
        } catch (IOException e) {
             e.printStackTrace();
        }
        eDriver  = new EventFiringWebDriver(new RemoteWebDriver(service.getUrl(),
                        DesiredCapabilities.chrome()));

//        eDriver.register(new MyEventListener());
        driver = eDriver;
    }

    public static WebDriver getDriver() {
        if(driver != null) {
            return driver;
        } else {
            new SimpleDriver();
            return driver;
        }
    }

}
