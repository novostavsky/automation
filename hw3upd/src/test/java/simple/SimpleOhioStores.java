package simple;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.epam.automation.pages.LandingPage;
import org.junit.jupiter.api.Test;

public class SimpleOhioStores {

    @Test
    public void multiplicationOfZeroIntegersShouldReturnZero() {
        LandingPage lp = new LandingPage();
        lp.openPage()
                .goToStore()
                .openUSStorePage()
                .selectState("Ohio");

        assertTrue(lp.isTextPresent("Beavercreek, OH 45440 4396 Holly Drive"),
                "Test address for Ohio...");
    }
}
