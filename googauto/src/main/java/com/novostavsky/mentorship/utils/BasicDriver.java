package com.novostavsky.mentorship.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BasicDriver {
    public static WebDriver init(){
        System.setProperty("webdriver.gecko.driver", Params.geckoPath);
        return new FirefoxDriver();
    }
}
