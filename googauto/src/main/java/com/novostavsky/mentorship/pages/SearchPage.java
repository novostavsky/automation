package com.novostavsky.mentorship.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchPage extends BasicPage {
    public String pageURL;

    public SearchPage(){
        this.pageURL = "http://www.google.com";
    }

    public SearchPage searchForString(final String searchString){
        this.getDriver().get(this.pageURL);

        WebElement element = this.getDriver().findElement(By.name("q"));
        element.sendKeys("Cheese!");

        System.out.println("Page title is: " + this.getDriver().getTitle());

        // Google's search is rendered dynamically with JavaScript.
        // Wait for the page to load, timeout after 10 seconds
        (new WebDriverWait(this.getDriver(), 10)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getTitle().toLowerCase().startsWith(searchString);
            }
        });
        System.out.println("Page title is: " + this.getDriver().getTitle());

        return this;
    }
}
