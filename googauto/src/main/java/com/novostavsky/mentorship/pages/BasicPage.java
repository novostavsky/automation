package com.novostavsky.mentorship.pages;


import com.novostavsky.mentorship.utils.BasicDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasicPage {
    private WebDriver driver;
    protected String pageURL;
    protected int timeoutInSeconds;

    public BasicPage() {
        this.driver = BasicDriver.init();
        this.pageURL = "http://www.google.com";
        this.timeoutInSeconds = 10;
    }

    public WebDriver getDriver(){
        return this.driver;
    }

    public BasicPage openPage(){
        this.getDriver().get(this.pageURL);
        return this;
    }

    public boolean isTextPresent(String textToCheck){
        return this.getDriver().getPageSource().contains(textToCheck);
    }

    public void quitDriver(){
        this.driver.quit();
    }
}
