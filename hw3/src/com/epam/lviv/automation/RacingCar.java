package com.epam.lviv.automation;

import java.util.ArrayList;

public class RacingCar implements Runnable, Comparable<RacingCar> {
    private ArrayList<Float> path;
    private ArrayList<Float> speed;
    private int raceTime;
    private final int deadline;

    public ArrayList<Float> getPath() {
        return path;
    }

    public ArrayList<Float> getSpeed() {
        return speed;
    }

    public int getRaceTime() {
        return raceTime;
    }

    public int getDeadline() {
        return deadline;
    }


    public RacingCar(int timeToLive){
        this.path = new ArrayList<Float>();
        this.path.add(new Float(0));

        this.speed = new ArrayList<Float>();
        this.speed.add((float) (Math.random()*1000));
        this.raceTime = 0;
        this.deadline = timeToLive;
    }

    @Override
    public void run() {
        while(this.raceTime < this.deadline){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            this.path.add(this.path.get(this.raceTime) + this.speed.get(this.raceTime));
            this.raceTime++;
            this.speed.add((float) (Math.random()*1000));
        }
    }

    @Override
    public int compareTo(RacingCar carToCompare) {
        return (int) (carToCompare.path.get(carToCompare.raceTime) - this.path.get(this.raceTime));
    }
}
