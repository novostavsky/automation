package com.epam.lviv.automation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        //init input args
        List<RacingCar> listOfCars = new ArrayList<RacingCar>();
        int N = 10;
        int time = 20;
        int top = 3;

        //run the race
        for(int i=0; i<N; i++){
            listOfCars.add(new RacingCar(time));
            (new Thread(listOfCars.get(i))).start();
        }
        Thread.sleep(1000*(time+1));//ToDo I guess we need some kind of sync threads here instead of wait

        //sort the cars by path after race
        Collections.sort(listOfCars);

        //output top 3 cars history
        for (int i=0; i<top; i++){
            System.out.println("car #" + (i+1));
            System.out.println("Max path = " + listOfCars.get(i).getPath().get(time));

            for(int t=0; t<listOfCars.get(i).getDeadline()+1; t++){
                System.out.println("time=" + t +
                        "; speed=" + listOfCars.get(i).getSpeed().get(t) +
                            "; path=" + listOfCars.get(i).getPath().get(t));
            }
        }
    }
}
