package calc;

import java.text.SimpleDateFormat;
import java.util.Scanner;


public class Main{

    public static void numberCheck(float inputNumber){
        if(Math.abs(inputNumber)>20){
            System.out.println("WARNING: the number is in incorrect range (should be: -20 <= x <= 20): " + inputNumber
                + "\nThe calculation will be done, despite of the warning...");
        }
    }

    public static void printHelp(){
        System.out.println("Simple calculator. Usage:" +
                "\n float<Enter>" +
                "\n operator (+-/*)<Enter>" +
                "\n float<Enter>" +
                "\nAll floats should be -20 <= x <= 20 ");
    }


    public static void main(String[] args){
        //print help
        Main.printHelp();

        //read paramerts from console
        Scanner sc = new Scanner(System.in);
        float x = sc.nextFloat();
        char sym = sc.next("[+-/*]").charAt(0);
        float y = sc.nextFloat();
        sc.close();

        //check business logic
        Main.numberCheck(x);
        Main.numberCheck(y);

        //calculate
        SimpleCalculator calculatorInstance = new SimpleCalculator(x, y, sym);
        String output = calculatorInstance.printCalculationToString();

        //write calculation to a file
        String file = (new SimpleDateFormat("yy-mm-dd hh-mm-ss")).
             format(calculatorInstance.getLastCalculation());
        SimpleWriter writer = new SimpleWriter(file, output);
        writer.writeResults();
    }
}
