package calc;

import java.util.Date;

public class SimpleCalculator {

    private float firstInput;
    private float secondInput;
    private char operatorSymbol;

    private Date lastCalculation;

    public SimpleCalculator() {
    }
    public SimpleCalculator(float firstInput, float secondInput, char operatorSymbol) {
        this.firstInput = firstInput;
        this.secondInput = secondInput;
        this.operatorSymbol = operatorSymbol;
    }

    public float getFirstInput() {
        return firstInput;
    }

    public Date getLastCalculation() {
        return lastCalculation;
    }

    public void setLastCalculation(Date lastCalculation) {
        this.lastCalculation = lastCalculation;
    }

    public void setFirstInput(float firstInput) {

        this.firstInput = firstInput;
    }

    public float getSecondInput() {
        return secondInput;
    }

    public void setSecondInput(float secondInput) {
        this.secondInput = secondInput;
    }

    public char getOperatorSymbol() {
        return operatorSymbol;
    }

    public void setOperatorSymbol(char operatorSymbol) {
        this.operatorSymbol = operatorSymbol;
    }

    public float doCalculate(){
        this.setLastCalculation(new Date());

        switch (this.getOperatorSymbol()){
            case '+':
                return this.getFirstInput() + this.getSecondInput();
            case '-':
                return this.getFirstInput() - this.getSecondInput();
            case '/':
                return this.getFirstInput() / this.getSecondInput();
            case '*':
                return this.getFirstInput() * this.getSecondInput();
            default:
                throw new RuntimeException("Unexpected operator symbol detected (should be one of + - * /): "
                        + this.operatorSymbol);
        }
    }

    public String printCalculationToString(){
        return (this.getFirstInput() + " "
                + this.getOperatorSymbol() + " "
                    + this.getSecondInput() + " = "
                        + this.doCalculate() );
    }

    public void printCalculationToConsole(){
        System.out.println(this.printCalculationToString());
    }

}
