package calc;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class SimpleWriter {
    private String fileName;
    private String dataToWrite;

    public SimpleWriter(String fileName, String dataToWrite) {
        this.fileName = fileName;
        this.dataToWrite = dataToWrite;
    }

    private File createResultFile(){
        File file = new File(this.fileName);
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public void writeResults(){
        File f = this.createResultFile();

        try{
            Writer w = new FileWriter(fileName);
            w.write(this.dataToWrite);
            w.close();

        }catch( IOException e){
            e.printStackTrace();
        }
    }

}
